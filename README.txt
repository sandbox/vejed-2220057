Description
-----------
This module adds HTML5 placeholder and pattern attributes support to Webform (4.0 and above).

Requirements
------------
Drupal 7.x
Webform 4.x or newer.

Installation
------------
1. Copy the entire webform_html5 directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Create or edit a webform node. On "Form components" edit form you will see two nwe inputs: "HTML5 Placeholder" and "HTML5 Pattern".

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/webform_html5

